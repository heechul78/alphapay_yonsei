import sqlite3
import datetime
from calendar import monthrange

class StatManager:
    PATH = "./alphapay.sqlite"
    STAT_PERIOD_AS_MONTH = 6.0 #통계 수집 기간 (월)
    NO_INPUT = -1 #입력이 없을때

    CATEGORY_CAFE = 1
    CATEGORY_BAKERY = 2
    CATEGORY_FASTFOOD = 3
    CATEGORY_RESTAURANT = 4
    CATEGORY_MART = 5
    CATEGORY_GASSTATION = 8
    CATEGORY_DUTYFREE = 11
    CATEGORY_BEAUTYSHOP = 13
    CATEGORY_CONVENIENT = 14
    CATEGORY_EDU = 19
    CATEGORY_ONLINESHOPPING = 21

    def __init__(self):
        self.con = sqlite3.connect(self.PATH)

    #평균 횟수 (연령, 성별)
    def get_avg_count(self, age, gender):
        cursor = self.con.cursor()
        sql = "SELECT AVG(user_pay_stat2.total_count) FROM user, user_pay_stat2 WHERE user.NO = user_pay_stat2.NO AND user.gender = %d AND user.age = %d;" % (gender, age)
        cursor.execute(sql)
        return cursor.fetchone()[0]

    #평균 금액 (연령, 성별)
    def get_avg_amount(self, age, gender):
        cursor = self.con.cursor()
        sql = "SELECT AVG(user_pay_stat2.total_amount) FROM user, user_pay_stat2 WHERE user.NO = user_pay_stat2.NO AND user.gender = %d AND user.age = %d;" % (gender, age)
        cursor.execute(sql)
        return cursor.fetchone()[0]
    
    #평균 횟수 쿼리 (모든 조건)
    def get_avg_count_with_conditions(self, category, age, gender, marriage, education, car_own, child, no_in_family, dual_job, monthly_salary):
        cursor = self.con.cursor()
        sql = "SELECT AVG(user_pay_stat2.cat_count_%d) FROM user, user_pay_stat2 WHERE user.NO = user_pay_stat2.NO" % category
        if age != self.NO_INPUT:
            sql += " AND user.age = %d" % age

        if gender != self.NO_INPUT:
            sql += " AND user.gender = %d" % gender

        if marriage != self.NO_INPUT:
            sql += " AND user.Marriage = %d" % marriage

        if education != self.NO_INPUT:
            sql += " AND user.education = %d" % education

        if child != self.NO_INPUT:
            sql += " AND user.child = %d" % child

        if no_in_family != self.NO_INPUT:
            sql += " AND user.no_in_family = %d" % no_in_family

        if car_own != self.NO_INPUT:
            sql += " AND user.car_own = %d" % car_own

        if dual_job != self.NO_INPUT:
            sql += " AND user.dual_job = %d" % dual_job
            
        if monthly_salary != self.NO_INPUT:
            sql += " AND user.monthly_salary = %d" % monthly_salary

        cursor.execute(sql)
        result = cursor.fetchone()[0]
        return 0 if result is None else result

    #평균 금액 쿼리 (모든 조건)
    def get_avg_amount_with_conditions(self, category, age, gender, marriage, education, car_own, child, no_in_family, dual_job, monthly_salary):
        cursor = self.con.cursor()
        sql = "SELECT AVG(user_pay_stat2.cat_amount_%d) FROM user, user_pay_stat2 WHERE user.NO = user_pay_stat2.NO" % category
        if age != self.NO_INPUT:
            sql += " AND user.age = %d" % age

        if gender != self.NO_INPUT:
            sql += " AND user.gender = %d" % gender

        if marriage != self.NO_INPUT:
            sql += " AND user.Marriage = %d" % marriage

        if education != self.NO_INPUT:
            sql += " AND user.education = %d" % education

        if child != self.NO_INPUT:
            sql += " AND user.child = %d" % child

        if no_in_family != self.NO_INPUT:
            sql += " AND user.no_in_family = %d" % no_in_family

        if car_own != self.NO_INPUT:
            sql += " AND user.car_own = %d" % car_own

        if dual_job != self.NO_INPUT:
            sql += " AND user.dual_job = %d" % dual_job
            
        if monthly_salary != self.NO_INPUT:
            sql += " AND user.monthly_salary = %d" % monthly_salary

        cursor.execute(sql)
        result = cursor.fetchone()[0]
        return 0 if result is None else result

    #이번달 남은 날짜 계산
    def get_left_days_ratio(self):
        now = datetime.datetime.now()
        mr = monthrange(now.year, now.month)
        return (float(mr[1]) - float(now.day)) / float(mr[1])

    #소비 항목별 남은 횟수 예측
    def get_left_count_in_this_month_with_conditions(self, apply_analysis, category, conditions):
        if apply_analysis:
            conditions = self.apply_analysis_for_count(category, conditions)

        return ((self.get_avg_count_with_conditions(category, conditions['age'], conditions['gender'], conditions['marriage'], conditions['education'], conditions['car_own'], conditions['child'], conditions['no_in_family'], conditions['dual_job'], conditions['monthly_salary']) / self.STAT_PERIOD_AS_MONTH) * self.get_left_days_ratio())

    #소비 항목별 남은 금액 예측
    def get_left_amount_in_this_month_with_conditions(self, apply_analysis, category, conditions):
        if apply_analysis:
            conditions = self.apply_analysis_for_amount(category, conditions)

        return ((self.get_avg_amount_with_conditions(category, conditions['age'], conditions['gender'], conditions['marriage'], conditions['education'], conditions['car_own'], conditions['child'], conditions['no_in_family'], conditions['dual_job'], conditions['monthly_salary']) / self.STAT_PERIOD_AS_MONTH) * self.get_left_days_ratio())

    #통계 분석 내용 적용
    def apply_analysis_for_amount(self, category, conditions):
        if category == self.CATEGORY_CAFE: #연령
            return {'age':conditions['age'], 'gender':self.NO_INPUT, 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_BAKERY: #결혼유무
            return {'age':self.NO_INPUT, 'gender':self.NO_INPUT, 'marriage':conditions['marriage'], 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_RESTAURANT: #성별, 평균수입
            return {'age':self.NO_INPUT, 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':conditions['monthly_salary']}
        elif category == self.CATEGORY_MART: #결혼유무, 차량유무
            return {'age':self.NO_INPUT, 'gender':self.NO_INPUT, 'marriage':conditions['marriage'], 'education':self.NO_INPUT, 'car_own':conditions['car_own'], 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_GASSTATION: #성별, 결혼유무, 차량유무
            return {'age':self.NO_INPUT, 'gender':conditions['gender'], 'marriage':conditions['marriage'], 'education':self.NO_INPUT, 'car_own':conditions['car_own'], 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_DUTYFREE: #성별, 연령, 맞벌이 여부, 자녀숫자, 가족숫자
            return {'age':conditions['age'], 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':conditions['child'], 'no_in_family':conditions['no_in_family'], 'dual_job':conditions['dual_job'], 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_BEAUTYSHOP: #성별, 연령, 자녀숫자
            return {'age':conditions['age'], 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':conditions['child'], 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_CONVENIENT: #성별, 연령, 맞벌이 여부
            return {'age':conditions['age'], 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':conditions['dual_job'], 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_EDU: #결혼유무, 평균수입
            return {'age':self.NO_INPUT, 'gender':self.NO_INPUT, 'marriage':conditions['marriage'], 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':conditions['monthly_salary']}
        elif category == self.CATEGORY_ONLINESHOPPING: # 자녀숫자
            return {'age':self.NO_INPUT, 'gender':self.NO_INPUT, 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT,  'child':conditions['child'], 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        else : return conditions

    def apply_analysis_for_count(self, category, conditions):
        if category == self.CATEGORY_CAFE: #연령, 차량유무
            return {'age':conditions['age'], 'gender':self.NO_INPUT, 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':conditions['car_own'], 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_BAKERY: #성별, 결혼유무
            return {'age':self.NO_INPUT, 'gender':conditions['gender'], 'marriage':conditions['marriage'], 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_FASTFOOD: #연령
            return {'age':conditions['age'], 'gender':self.NO_INPUT, 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':conditions['monthly_salary']}
        elif category == self.CATEGORY_RESTAURANT: #성별
            return {'age':self.NO_INPUT, 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_GASSTATION: #성별, 결혼유무, 차량유무, 평균수입
            return {'age':self.NO_INPUT, 'gender':conditions['gender'], 'marriage':conditions['marriage'], 'education':self.NO_INPUT, 'car_own':conditions['car_own'], 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':conditions['monthly_salary']}
        elif category == self.CATEGORY_DUTYFREE: #성별, 자녀숫자, 가족숫자
            return {'age':conditions['age'], 'gender':self.NO_INPUT, 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':conditions['child'], 'no_in_family':conditions['no_in_family'], 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_BEAUTYSHOP: #연령, 자녀숫자
            return {'age':self.NO_INPUT, 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':conditions['child'], 'no_in_family':self.NO_INPUT, 'dual_job':self.NO_INPUT, 'monthly_salary':self.NO_INPUT}
        elif category == self.CATEGORY_CONVENIENT: #성별, 맞벌이 여부
            return {'age':conditions['age'], 'gender':conditions['gender'], 'marriage':self.NO_INPUT, 'education':self.NO_INPUT, 'car_own':self.NO_INPUT, 'child':self.NO_INPUT, 'no_in_family':self.NO_INPUT, 'dual_job':conditions['dual_job'], 'monthly_salary':self.NO_INPUT}
        else : return conditions
    
#test 코드
def test():
    statmanager = StatManager()
    print("통계 적용: 30대, 남성, 기혼, 대졸, 차보유, 월수 400~500, 이번달에 교육에 얼마나 더 카드를 사용할 지 총 금액 : %f원" % statmanager.get_left_amount_in_this_month_with_conditions(True, 19, {'age':3, 'gender':1, 'marriage':1, 'education':3, 'car_own':1, 'child':0, 'no_in_family':2, 'dual_job':0, 'monthly_salary':4}))
    print("통계 적용: 20대, 남성, 기혼, 대졸, 차보유,  월수 300~400, 이번달에 편의점에 얼마나 더 카드를 사용할 지 총 횟수 : %f회" % statmanager.get_left_count_in_this_month_with_conditions(True, 14, {'age':2, 'gender':1, 'marriage':1, 'education':3, 'car_own':1, 'child':0, 'no_in_family':2, 'dual_job':0, 'monthly_salary':3}))
    print("통계 적용: 20대, 여성, 기혼, 대졸, 차보유,  월수 300~400, 이번달에 카페에 얼마나 더 카드를 사용할 지 총 금액 : %f원" % statmanager.get_left_amount_in_this_month_with_conditions(True, 1, {'age':2, 'gender':0, 'marriage':1, 'education':3, 'car_own':1, 'child':0, 'no_in_family':2, 'dual_job':0, 'monthly_salary':3}))
    print("통계 적용: 30대, 남성, 기혼, 대졸, 차보유,  월수 400~500, 이번달에 주유소에 얼마나 더 카드를 사용할 지 총 횟수 : %f회" % statmanager.get_left_count_in_this_month_with_conditions(True, 14, {'age':3, 'gender':1, 'marriage':1, 'education':3, 'car_own':1, 'child':0, 'no_in_family':2, 'dual_job':0, 'monthly_salary':4}))
    print("통계 적용: 40대, 남성, 기혼, 대졸, 차보유,  월수 400~500, 이번달에 마트에 얼마나 더 카드를 사용할 지 총 금액 : %f원" % statmanager.get_left_amount_in_this_month_with_conditions(True, 5, {'age':4, 'gender':1, 'marriage':1, 'education':3, 'car_own':1, 'child':0, 'no_in_family':2, 'dual_job':0, 'monthly_salary':4}))
    print("통계 적용: 30대, 여성, 기혼, 대졸, 차보유,  월수 300~400, 이번달에 베이커리에 얼마나 더 카드를 사용할 지 총 횟수 : %f회" % statmanager.get_left_count_in_this_month_with_conditions(True, 2, {'age':2, 'gender':0, 'marriage':1, 'education':3, 'car_own':1, 'child':0, 'no_in_family':2, 'dual_job':0, 'monthly_salary':3}))
#test 코드 실행
test()

"""
gender 0:여자, 1:남자
marriage 미혼: 0, 1:결혼
education 1:고졸이하, 2:전문대, 3:대졸, 4:대학원졸, 5:박사
dual_job (맞벌이) 0:맞벌이아님, 1:맞벌이
monthly_salary : 100~200 : 1, 200~300 : 2, 300~400 : 3, 400~500 : 4, 500~ : 5

user structure
NO	UID	Gender	Age	Education	Marriage	Dual_job	Child	No_in_family	Living_city	Living_gu	Working_city	Working_gu	Monthly_salary	Car_own	No_of_devices	No_of_credit_cards	No_of_check_cards	No_of_direct_cards	No_of_prepaid_cards

user_pay_stat2 table structure
cat_count_1	cat_amount_1	cat_count_2	cat_amount_2	cat_count_3	cat_amount_3	cat_count_4	cat_amount_4	cat_count_5	cat_amount_5	cat_count_6	cat_amount_6	cat_count_7	cat_amount_7	cat_count_8	cat_amount_8	cat_count_9	cat_amount_9	cat_count_10	cat_amount_10	cat_count_11	cat_amount_11	cat_count_12	cat_amount_12	cat_count_13	cat_amount_13	cat_count_14	cat_amount_14	cat_count_15	cat_amount_15	cat_count_16	cat_amount_16	cat_count_17	cat_amount_17	cat_count_18	cat_amount_18	cat_count_19	cat_amount_19	cat_count_20	cat_amount_20	cat_count_21	cat_amount_21	cat_count_22	cat_amount_22	cat_count_23	cat_amount_23	cat_count_24	cat_amount_24	cat_count_25	cat_amount_25	cat_count_26	cat_amount_26	cat_count_27	cat_amount_27	cat_count_28	cat_amount_28	cat_count_29	cat_amount_29

category code
1	카페
2	베이커리/아이스크림
3	패스트푸드
4	음식점
5	마트
6	영화
7	서점
8	주유소
9	백화점
10	아울렛
11	면세점
12	의료
13	뷰티샵
14	편의점
15	의류
16	미용실
17	여가_취미
18	놀이공원
19	교육
20	여행
21	온라인쇼핑
22	소셜커머스
23	항공
24	교통
25	육아
26	통신
27	생활
28	해외직구
29	기타
"""
